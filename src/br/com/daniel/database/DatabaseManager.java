package br.com.daniel.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import br.com.daniel.model.Cliente;
import br.com.daniel.model.Compra;
import br.com.daniel.model.ItemCompra;
import br.com.daniel.model.Produto;

public class DatabaseManager {

	//Métodos de escrita
	public int salvar(Cliente c) {
		int res=0;

		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("insert into daniel.cliente (nome) values (?)");
			ps.setString(1, c.getNome());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return res;
	}

	public Vector<Integer> salvar(Compra c) {
		int res=0;
		int chavePrimaria=0;

		try {
			String key[] = {"id"};//Campo da chave primaria a ser retornado ao inserir a compra
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("insert into daniel.compra (idcliente) values (?)", key);
			ps.setInt(1, c.getIdCliente());
			res = ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next()) {
				chavePrimaria = rs.getInt(1);
			}

			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		Vector<Integer> retorno = new Vector<Integer>();
		retorno.add(res);
		retorno.add(chavePrimaria);

		return retorno;
	}

	public int salvar(ItemCompra i) {
		int res=0;

		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("insert into daniel.item_compra (idcompra, idproduto, quantidade) values (?,?,?)");
			ps.setInt(1, i.getIdCompra());
			ps.setInt(2, i.getIdProduto());
			ps.setInt(3, i.getQuantidade());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return res;
	}

	public int salvar(Produto p) {
		int res=0;

		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("insert into daniel.produto (nome, valor) values (?,?)");
			ps.setString(1, p.getNomeProduto());
			ps.setFloat(2, p.getValorProduto());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		return res;
	}

	//Métodos de carregamento
	public Vector<Cliente> carregarTodosClientes() {

		Vector<Cliente> clientes= new Vector<Cliente>();

		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM daniel.cliente ORDER BY nome");
			while (rs.next()) {
				Cliente temp = new Cliente();
				temp.setId(rs.getInt(1));
				temp.setNome(rs.getString(2));
				clientes.add(temp);
			}
			stm.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return clientes;
	}


	public Vector<Produto> carregarTodosProdutos() {

		Vector<Produto> produtos= new Vector<Produto>();

		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM daniel.produto ORDER BY nome");
			while (rs.next()) {
				Produto temp = new Produto();
				temp.setId(rs.getInt(1));
				temp.setNomeProduto(rs.getString(2));
				temp.setValorProduto(rs.getFloat(3));
				produtos.add(temp);
			}
			stm.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return produtos;
	}

	public Vector<Compra> carregarTodasCompras() {

		Vector<Compra> compras = new Vector<Compra>();

		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("SELECT c.id, c.idcliente, u.nome as nomecliente, (SELECT COUNT (id) FROM daniel.item_compra WHERE idcompra = c.id) AS totalprodutos FROM daniel.compra AS c INNER JOIN daniel.cliente as u ON (c.idcliente = u.id)");
			while (rs.next()) {
				Compra temp = new Compra();
				temp.setId(rs.getInt(1));
				temp.setIdCliente(rs.getInt(2));
				temp.setNomeCliente(rs.getString(3));
				temp.setTotalProdutos(rs.getInt(4));
				compras.add(temp);
			}
			stm.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return compras;
	}
	
	public Compra carregarCompra(int idCompra) {
		
		Compra compra = new Compra();
		
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("SELECT c.id, c.idcliente, u.nome as nomecliente, (SELECT COUNT (id) FROM daniel.item_compra WHERE idcompra = c.id) AS totalprodutos FROM daniel.compra AS c INNER JOIN daniel.cliente as u ON (c.idcliente = u.id) WHERE c.id = "+idCompra+" ");
			while (rs.next()) {
				compra.setId(rs.getInt(1));
				compra.setIdCliente(rs.getInt(2));
				compra.setNomeCliente(rs.getString(3));
				compra.setTotalProdutos(rs.getInt(4));
			}
			stm.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return compra;
	}
	
	public Vector<ItemCompra> carregarItensCompra(Compra compra){
		
		Vector<ItemCompra> itensCompra = new Vector<ItemCompra>();
		
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("SELECT i.id, i.idcompra, i.idproduto, p.nome AS nomeproduto, i.quantidade FROM daniel.item_compra AS i INNER JOIN daniel.produto AS p ON (i.idproduto = p.id)  WHERE idcompra = "+compra.getId()+"");
			while (rs.next()) {
				ItemCompra temp = new ItemCompra();
				temp.setId(rs.getInt(1));
				temp.setIdCompra(rs.getInt(2));
				temp.setIdProduto(rs.getInt(3));
				temp.setNomeProduto(rs.getString(4));
				temp.setQuantidade(rs.getInt(5));
				itensCompra.add(temp);
			}
			stm.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return itensCompra;
		
	}


	//Métodos de Exclusão
	public int delete(Cliente c) {
		int res=0;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("delete from daniel.cliente where id=?");	
			ps.setInt(1, c.getId());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return res;
	}

	public int delete(Compra c) {
		int res=0;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("delete from daniel.compra where id=?");	
			ps.setInt(1, c.getId());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return res;
	}

	public int delete(ItemCompra i) {
		int res=0;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("delete from daniel.item_compra where id=?");	
			ps.setInt(1, i.getId());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return res;
	}
	public int delete(Produto p) {
		int res=0;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("delete from daniel.produto where id=?");	
			ps.setInt(1, p.getId());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return res;
	}

	//Métodos de Atualização/Update
	public int update(Cliente c) {
		int res=0;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("update daniel.cliente set nome=? where id=?");
			ps.setString(1, c.getNome());
			ps.setInt(2, c.getId());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return res;
	}


	public int update(Produto p) {
		int res=0;
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement ps = conn.prepareStatement("update daniel.produto set nome=?, valor=? where id=?");	
			ps.setString(1, p.getNomeProduto());
			ps.setFloat(2, p.getValorProduto());
			ps.setInt(3, p.getId());
			res = ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}		
		return res;
	}


}

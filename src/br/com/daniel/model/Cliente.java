package br.com.daniel.model;

public class Cliente {
	
	private int id;
	private String nomeCliente;
	
	public void setId(int elementId) {
		id = elementId;
	}
	
	public int getId() {
		return id;
	}
	
	public void setNome(String nome){
		nomeCliente = nome;
	}
	
	public String getNome() {
		return nomeCliente;
	}
	
}

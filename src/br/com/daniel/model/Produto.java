package br.com.daniel.model;

public class Produto {
	
	private int id;
	private String nomeProduto;
	private Float valorProduto;
	
	public void setId(int elementId) {
		id = elementId;
	}
	
	public int getId() {
		return id;
	}
	
	public void setNomeProduto(String nome){
		nomeProduto = nome;
	}
	
	public void setValorProduto(Float valor){
		valorProduto = valor;
	}
	
	public String getNomeProduto() {
		return nomeProduto;
	}
	
	public Float getValorProduto() {
		return valorProduto;
	}
	
}

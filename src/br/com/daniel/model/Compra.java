package br.com.daniel.model;

public class Compra {
	
	private int id;
	private int idCliente;
	private String nomeCliente;
	private int totalProdutos;
	
	public void setId(int elementId) {
		id = elementId;
	}
	
	public int getId() {
		return id;
	}
	
	public void setIdCliente(int id){
		idCliente = id;
	}
	
	public int getIdCliente() {
		return idCliente;
	}
	
	public void setNomeCliente(String nome) {
		nomeCliente = nome;
	}
	
	public String getNomeCliente() {
		return nomeCliente;
	}
	
	public void setTotalProdutos(int total){
		totalProdutos = total;
	}
	
	public int getTotalProdutos() {
		return totalProdutos;
	}
	
}

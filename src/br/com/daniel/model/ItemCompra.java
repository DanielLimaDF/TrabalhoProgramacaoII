package br.com.daniel.model;

public class ItemCompra {
	
	private int id;
	private int idCompra;
	private int idProduto;
	private String nomeProduto;
	private int quantidade;
	
	public void setId(int elementId) {
		id = elementId;
	}
	
	public int getId() {
		return id;
	}
	
	public void setIdCompra(int id){
		idCompra = id;
	}
	
	public void setIdProduto(int id){
		idProduto = id;
	}
	
	public int getIdCompra() {
		return idCompra;
	}
	
	public int getIdProduto() {
		return idProduto;
	}
	
	public void setNomeProduto(String nome) {
		nomeProduto = nome;
	}
	
	public String getNomeProduto() {
		return nomeProduto;
	}
	
	public void setQuantidade(int total) {
		quantidade = total;
	}
	
	public int getQuantidade() {
		return quantidade;
	}
	
}

package br.com.daniel.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import br.com.daniel.database.DatabaseManager;
import br.com.daniel.model.Cliente;
import br.com.daniel.model.Compra;
import br.com.daniel.model.ItemCompra;
import br.com.daniel.model.Produto;

public class JanelaEditCompras extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4805598044434852269L;
	private JPanel painel;
	private JPanel painelTitulo;
	private JPanel painelCadastro;
	private JPanel painelListaItensCompra;
	private JPanel painelAcaoConcluir;
	private JLabel labelTitulo;
	private JLabel labelFieldProduto;
	private JLabel labelFieldQuantidade;
	private JComboBox<String> comboBoxProdutos;
	private JTextField textFieldQuantidade;
	private JButton btnAdicionar;
	private JButton btnRemover;
	private JButton btnConcluir;
	private JScrollPane scrollPane;
	private JList<String> listaItensCompra;
	private DatabaseManager gerenciadorBancoDeDados;
	private Vector<Produto> produtosCadastrados;
	private Vector<ItemCompra> itensCadastrados;
	private Compra compra;

	public JanelaEditCompras(Compra compraEditar) {
		super("Editar Compra");

		//Banco
		gerenciadorBancoDeDados = new DatabaseManager();
		compra = compraEditar;

		//Iniciar elementos da janela
		painel = new JPanel();
		painelTitulo = new JPanel();
		painelCadastro = new JPanel();
		painelListaItensCompra = new JPanel();
		painelAcaoConcluir = new JPanel();
		labelTitulo = new JLabel("Adicionar produtos nesta compra");
		labelFieldProduto = new JLabel("Produto: ");
		labelFieldQuantidade = new JLabel("Quantidade: ");
		comboBoxProdutos = new JComboBox<>();
		textFieldQuantidade = new JTextField("1", 6);
		btnAdicionar = new JButton("+ Adicionar");
		btnRemover = new JButton("- Remover item");
		btnConcluir = new JButton("Concluir");
		scrollPane = new JScrollPane();
		listaItensCompra = new JList<String>();

		//Configurar painel/janela
		painel.setLayout(new GridLayout(4, 1));
		painelTitulo.setLayout(new FlowLayout());
		painelCadastro.setLayout(new FlowLayout());
		painelListaItensCompra.setLayout(new GridLayout(1, 1));
		painelAcaoConcluir.setLayout(new GridLayout());
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(painel, BorderLayout.CENTER);

		//Adicionar elementos
		painel.add(painelTitulo);
		painel.add(painelCadastro);
		painel.add(painelListaItensCompra);
		painel.add(painelAcaoConcluir);

		painelTitulo.add(labelTitulo);

		painelCadastro.add(labelFieldProduto);
		painelCadastro.add(comboBoxProdutos);
		painelCadastro.add(labelFieldQuantidade);
		painelCadastro.add(textFieldQuantidade);
		painelCadastro.add(btnAdicionar);

		painelListaItensCompra.add(scrollPane);
		scrollPane.setViewportView(listaItensCompra);

		painelAcaoConcluir.add(btnRemover);
		painelAcaoConcluir.add(btnConcluir);

		//Configurações da janela
		setSize(500, 400);
		setLocationRelativeTo(null);

		//Mostrar janela
		setVisible(true);


		//Ações

		//Cadastrar novo ítem para a compra
		btnAdicionar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				//Cliente selecionado
				int indexProdutoSelecionado = comboBoxProdutos.getSelectedIndex();
				Produto produtoSelecionado = produtosCadastrados.elementAt(indexProdutoSelecionado);

				ItemCompra novoitem = new ItemCompra();
				novoitem.setIdCompra(compra.getId());
				novoitem.setIdProduto(produtoSelecionado.getId());
				novoitem.setQuantidade(Integer.valueOf(textFieldQuantidade.getText()));

				int result = gerenciadorBancoDeDados.salvar(novoitem);

				if(result != 0) {
					JOptionPane.showMessageDialog(null, "Item adicionado com sucesso!");
					carregarItensCompra();

				}else {
					JOptionPane.showMessageDialog(null, "Erro no cadastro! Tente novamente");
				}

			}
		});


		//Concluir tarefa de gerenciamento de compra
		btnConcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				dispose();

			}
		});

		//Remover produto da compra excluindo o ItemProduto do banco de dados
		btnRemover.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int indexSelecionado = listaItensCompra.getSelectedIndex();

				int result = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja REMOVER o produto selecionado desta compra?", "Aviso", JOptionPane.YES_NO_OPTION);

				//Excluir caso o sim seja escolhido
				if(result == JOptionPane.YES_OPTION) {

					ItemCompra itemExcluir = itensCadastrados.elementAt(indexSelecionado);
					
					int resultExlusion = gerenciadorBancoDeDados.delete(itemExcluir);

					if(resultExlusion != 0){
						JOptionPane.showMessageDialog(null, "Item removido com sucesso!");
					}

					carregarItensCompra();

				}

			}
		});


		carregarProdutos();
		carregarItensCompra();

	}








	//Carregar lista de clientes do banco de dados web
	public void carregarProdutos() {

		produtosCadastrados = gerenciadorBancoDeDados.carregarTodosProdutos();

		for (Produto p : produtosCadastrados) {
			//System.out.println(c.getNome());
			comboBoxProdutos.addItem(p.getNomeProduto());
		}

	}

	//Carregar lista de itens desta compra
	public void carregarItensCompra() {

		itensCadastrados = gerenciadorBancoDeDados.carregarItensCompra(compra);

		listaItensCompra.removeAll();

		DefaultListModel<String> listModel = new DefaultListModel<String>();

		for (ItemCompra i : itensCadastrados) {
			listModel.addElement(i.getNomeProduto() + " [Quantidade: "+ i.getQuantidade() +"]");
		}

		listaItensCompra.setModel(listModel);
		listaItensCompra.setSelectedIndex(0);

	}

}



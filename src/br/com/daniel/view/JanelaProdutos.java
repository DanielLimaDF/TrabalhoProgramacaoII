package br.com.daniel.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import br.com.daniel.database.DatabaseManager;
import br.com.daniel.model.Cliente;
import br.com.daniel.model.Produto;

public class JanelaProdutos extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5227494689547253999L;
	private JPanel painel;
	private JPanel painelTitulo;
	private JPanel painelCadastro;
	private JPanel painelProdutos;
	private JPanel painelAcoes;
	private JLabel labelTitulo;
	private JLabel labelFieldNome;
	private JTextField textFieldNomeProduto;
	private JLabel labelFieldValor;
	private JTextField textFieldValorProduto;
	private JButton btnCadastrar;
	private JButton btnEditar;
	private JButton btnAtualizar;
	private JButton btnExcluir;
	private JScrollPane scrollPane;
	private JList<String> listaProdutos;
	private DatabaseManager gerenciadorBancoDeDados;
	private Vector<Produto> produtosCadastrados;
	
	public JanelaProdutos() {
		super("Produtos");
		
		//Banco
		gerenciadorBancoDeDados = new DatabaseManager();
		
		//Iniciar elementos da janela
		painel = new JPanel();
		painelTitulo = new JPanel();
		painelCadastro = new JPanel();
		painelProdutos = new JPanel();
		painelAcoes = new JPanel();
		labelTitulo = new JLabel("Gerenciar Produtos");
		labelFieldNome = new JLabel("Nome: ");
		textFieldNomeProduto = new JTextField("", 12);
		labelFieldValor = new JLabel("Valor: ");
		textFieldValorProduto = new JTextField("0.00", 6);
		btnCadastrar = new JButton("Cadastrar");
		btnEditar = new JButton("Editar");
		btnAtualizar = new JButton("Recarregar");
		btnExcluir = new JButton("Excluir");
		scrollPane = new JScrollPane();
		listaProdutos = new JList<String>();
		
		//Configurar painel/janela
		painel.setLayout(new GridLayout(4, 1));
		painelTitulo.setLayout(new FlowLayout());
		painelCadastro.setLayout(new FlowLayout());
		painelProdutos.setLayout(new GridLayout(1, 1));
		painelAcoes.setLayout(new GridLayout());
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(painel, BorderLayout.CENTER);
		
		//Adicionar elementos
		painel.add(painelTitulo);
		painel.add(painelCadastro);
		painel.add(painelProdutos);
		painel.add(painelAcoes);
		
		painelTitulo.add(labelTitulo);
		
		painelCadastro.add(labelFieldNome);
		painelCadastro.add(textFieldNomeProduto);
		painelCadastro.add(labelFieldValor);
		painelCadastro.add(textFieldValorProduto);
		painelCadastro.add(btnCadastrar);
		
		painelProdutos.add(scrollPane);
		scrollPane.setViewportView(listaProdutos);
		
		painelAcoes.add(btnEditar);
		painelAcoes.add(btnAtualizar);
		painelAcoes.add(btnExcluir);
		
		//Configurações da janela
		setSize(500, 400);
		setLocationRelativeTo(null);
		
		//Mostrar janela
		setVisible(true);
		
		
		//Adicionar ações
		
		//Cadastrar novo produto
		btnCadastrar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if(textFieldNomeProduto.getText().equals("")) {

					JOptionPane.showMessageDialog(null, "Por favor informe o nome do produto");

				}else {

					Produto novoProduto = new Produto();
					novoProduto.setNomeProduto(textFieldNomeProduto.getText());
					novoProduto.setValorProduto(Float.valueOf(textFieldValorProduto.getText()));
					
					int result = gerenciadorBancoDeDados.salvar(novoProduto);
					
					if(result != 0) {
						JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso!");
						carregarProdutos();
					}else {
						JOptionPane.showMessageDialog(null, "Erro no cadastro! Tente novamente");
					}

				}

			}
		});

		//Editar produto
		btnEditar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				int indexSelecionado = listaProdutos.getSelectedIndex();
				Produto produtoEditar = produtosCadastrados.elementAt(indexSelecionado);
				
				JanelaEditProduto jep = new JanelaEditProduto(produtoEditar);
				
			}
		});

		//Recarregar lista de produtos
		btnAtualizar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				carregarProdutos();

			}
		});

		//Excluir produto selecionado
		btnExcluir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int indexSelecionado = listaProdutos.getSelectedIndex();

				int result = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja APAGAR o produto selecionado?", "Aviso", JOptionPane.YES_NO_OPTION);

				//Excluir caso o sim seja escolhido
				if(result == JOptionPane.YES_OPTION) {

					Produto produtoExcluir = produtosCadastrados.elementAt(indexSelecionado);

					int resultExlusion = gerenciadorBancoDeDados.delete(produtoExcluir);

					if(resultExlusion != 0){
						JOptionPane.showMessageDialog(null, "Produto excluído com sucesso!");
					}

					carregarProdutos();

				}

			}
		});


		//Carregar clientes
		carregarProdutos();
		
		
	}
	
	
	//Carregar lista de clientes do banco de dados web
	public void carregarProdutos() {
		
		produtosCadastrados = gerenciadorBancoDeDados.carregarTodosProdutos();
		
		//Limpar jlist
		listaProdutos.removeAll();
		
		DefaultListModel<String> listModel = new DefaultListModel<String>();
		
		for (Produto p : produtosCadastrados) {
			listModel.addElement(p.getNomeProduto() + " ["+ p.getValorProduto() +"]");
		}
		
		listaProdutos.setModel(listModel);
		listaProdutos.setSelectedIndex(0);
		
	}
	
	
}

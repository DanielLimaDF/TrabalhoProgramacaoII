package br.com.daniel.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import br.com.daniel.database.DatabaseManager;
import br.com.daniel.model.Cliente;

public class JanelaClientes extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1205404955741883156L;
	private JPanel painel;
	private JPanel painelTitulo;
	private JPanel painelCadastro;
	private JPanel painelClientes;
	private JPanel painelAcoes;
	private JLabel labelTitulo;
	private JLabel labelFieldNome;
	private JTextField textFieldNomeCliente;
	private JButton btnCadastrar;
	private JButton btnEditar;
	private JButton btnAtualizar;
	private JButton btnExcluir;
	private JScrollPane scrollPane;
	private JList<String> listaClientes;
	private DatabaseManager gerenciadorBancoDeDados;
	private Vector<Cliente> clientesCadastrados;
	
	public JanelaClientes() {
		super("Clientes");
		
		//Banco
		gerenciadorBancoDeDados = new DatabaseManager();
		
		//Iniciar elementos da janela
		painel = new JPanel();
		painelTitulo = new JPanel();
		painelCadastro = new JPanel();
		painelClientes = new JPanel();
		painelAcoes = new JPanel();
		labelTitulo = new JLabel("Gerenciar Clientes");
		labelFieldNome = new JLabel("Nome: ");
		textFieldNomeCliente = new JTextField("", 20);
		btnCadastrar = new JButton("Cadastrar");
		btnEditar = new JButton("Editar");
		btnAtualizar = new JButton("Recarregar");
		btnExcluir = new JButton("Excluir");
		scrollPane = new JScrollPane();
		listaClientes = new JList<String>();
		
		//Configurar painel/janela
		painel.setLayout(new GridLayout(4, 1));
		painelTitulo.setLayout(new FlowLayout());
		painelCadastro.setLayout(new FlowLayout());
		painelClientes.setLayout(new GridLayout(1, 1));
		painelAcoes.setLayout(new GridLayout());
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(painel, BorderLayout.CENTER);
		
		//Adicionar elementos
		painel.add(painelTitulo);
		painel.add(painelCadastro);
		painel.add(painelClientes);
		painel.add(painelAcoes);
		
		painelTitulo.add(labelTitulo);
		
		painelCadastro.add(labelFieldNome);
		painelCadastro.add(textFieldNomeCliente);
		painelCadastro.add(btnCadastrar);
		
		painelClientes.add(scrollPane);
		scrollPane.setViewportView(listaClientes);
		
		painelAcoes.add(btnEditar);
		painelAcoes.add(btnAtualizar);
		painelAcoes.add(btnExcluir);
		
		//Configurações da janela
		setSize(500, 400);
		setLocationRelativeTo(null);
		
		//Mostrar janela
		setVisible(true);
		
		
		//Adicionar ações
		
		//Cadastrar novo cliente
		btnCadastrar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(textFieldNomeCliente.getText().equals("")) {
					
					JOptionPane.showMessageDialog(null, "Por favor informe o nome do cliente");
					
				}else {
					
					Cliente novoCliente = new Cliente();
					novoCliente.setNome(textFieldNomeCliente.getText());
					
					int result = gerenciadorBancoDeDados.salvar(novoCliente);
					
					if(result != 0) {
						JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso!");
						carregarClientes();
					}else {
						JOptionPane.showMessageDialog(null, "Erro no cadastro! Tente novamente");
					}
					
				}
				
			}
		});
		
		//Editar cliente
		btnEditar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				int indexSelecionado = listaClientes.getSelectedIndex();
				Cliente clienteEditar = clientesCadastrados.elementAt(indexSelecionado);
				
				JanelaEditCliente jec = new JanelaEditCliente(clienteEditar);
				
			}
		});
		
		//Recarregar lista de clientes
		btnAtualizar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				carregarClientes();
				
			}
		});
		
		//Excluir cliente selecionado
		btnExcluir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				int indexSelecionado = listaClientes.getSelectedIndex();
				
				int result = JOptionPane.showConfirmDialog(null, "Tem certeza que deseja APAGAR o cliente selecionado?", "Aviso", JOptionPane.YES_NO_OPTION);
				
				//Excluir caso o sim seja escolhido
				if(result == JOptionPane.YES_OPTION) {
					
					Cliente clienteExcluir = clientesCadastrados.elementAt(indexSelecionado);
					
					int resultExlusion = gerenciadorBancoDeDados.delete(clienteExcluir);
					
					if(resultExlusion != 0){
						JOptionPane.showMessageDialog(null, "Cliente excluído com sucesso!");
					}
					
					carregarClientes();
					
				}
				
			}
		});
		
		
		//Carregar clientes
		carregarClientes();
		
		
	}
	
	
	//Carregar lista de clientes do banco de dados web
	public void carregarClientes() {
		
		clientesCadastrados = gerenciadorBancoDeDados.carregarTodosClientes();
		
		//Limpar jlist
		listaClientes.removeAll();
		
		DefaultListModel<String> listModel = new DefaultListModel<String>();
		
		for (Cliente c : clientesCadastrados) {
			//System.out.println(c.getNome());
			listModel.addElement(c.getNome());
		}
		
		listaClientes.setModel(listModel);
		listaClientes.setSelectedIndex(0);
		
	}
	
	
}

package br.com.daniel.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.com.daniel.database.DatabaseManager;
import br.com.daniel.model.Cliente;

public class JanelaEditCliente extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3469330370488612502L;
	private JPanel painel;
	private JPanel painelTitulo;
	private JPanel painelEdicao;
	private JPanel painelAcaoSalvar;
	private JLabel labelTitulo;
	private JLabel labelFieldNome;
	private JTextField textFieldNomeCliente;
	private JButton btnSalvar;
	private DatabaseManager gerenciadorBancoDeDados;
	private Cliente cliente;
	
	public JanelaEditCliente(Cliente clienteEditar){
		super("Editar Cliente");

		//Banco
		gerenciadorBancoDeDados = new DatabaseManager();

		//Iniciar elementos da janela
		painel = new JPanel();
		painelTitulo = new JPanel();
		painelEdicao = new JPanel();
		painelAcaoSalvar = new JPanel();
		labelTitulo = new JLabel("Gerenciar Clientes");
		labelFieldNome = new JLabel("Nome: ");
		textFieldNomeCliente = new JTextField("", 20);
		btnSalvar = new JButton("Salvar alterações");
		
		cliente = clienteEditar;
		
		//Configurar painel/janela
		painel.setLayout(new GridLayout(4, 1));
		painelTitulo.setLayout(new FlowLayout());
		painelEdicao.setLayout(new FlowLayout());
		painelAcaoSalvar.setLayout(new GridLayout());
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(painel, BorderLayout.CENTER);
		
		//Adicionar elementos
		painel.add(painelTitulo);
		painel.add(painelEdicao);
		painel.add(painelAcaoSalvar);
		
		painelTitulo.add(labelTitulo);
		
		painelEdicao.add(labelFieldNome);
		painelEdicao.add(textFieldNomeCliente);
		
		painelAcaoSalvar.add(btnSalvar);
		
		//Configurações da janela
		setSize(500, 200);
		setLocationRelativeTo(null);
		
		//Mostrar janela
		setVisible(true);
		
		//Preencher campos
		textFieldNomeCliente.setText(cliente.getNome());
		
		//Ação de atualização
		btnSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if(textFieldNomeCliente.getText().equals("")) {

					JOptionPane.showMessageDialog(null, "Por favor informe o nome do cliente");

				}else {
					
					cliente.setNome(textFieldNomeCliente.getText());

					int result = gerenciadorBancoDeDados.update(cliente);

					if(result != 0) {
						JOptionPane.showMessageDialog(null, "Cliente atualizado com sucesso!");
						dispose();
					}else {
						JOptionPane.showMessageDialog(null, "Erro no update! Tente novamente");
					}

				}

			}
		});

		
	}

}

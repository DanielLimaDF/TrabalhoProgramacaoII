package br.com.daniel.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import br.com.daniel.database.DatabaseManager;
import br.com.daniel.model.Cliente;
import br.com.daniel.model.Compra;
import br.com.daniel.model.Produto;

public class JanelaCompras extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8137666811060288772L;
	private JPanel painel;
	private JPanel painelTitulo;
	private JPanel painelCadastro;
	private JPanel painelCompras;
	private JPanel painelAcoes;
	private JLabel labelTitulo;
	private JLabel labelFieldCliente;
	private JComboBox<String> comboBoxClientes;
	private JButton btnCadastrar;
	private JButton btnEditar;
	private JButton btnAtualizar;
	private JScrollPane scrollPane;
	private JList<String> listaCompras;
	private DatabaseManager gerenciadorBancoDeDados;
	private Vector<Cliente> clientesCadastrados;
	private Vector<Compra> comprasCadastradas;

	public JanelaCompras() {
		super("Compras");

		//Banco
		gerenciadorBancoDeDados = new DatabaseManager();

		//Iniciar elementos da janela
		painel = new JPanel();
		painelTitulo = new JPanel();
		painelCadastro = new JPanel();
		painelCompras = new JPanel();
		painelAcoes = new JPanel();
		labelTitulo = new JLabel("Gerenciar Compras");
		labelFieldCliente = new JLabel("Clientes");
		comboBoxClientes = new JComboBox<>();
		btnCadastrar = new JButton("+ Nova Compra");
		btnEditar = new JButton("Editar");
		btnAtualizar = new JButton("Atualizar lista");
		scrollPane = new JScrollPane();
		listaCompras = new JList<String>();

		//Configurar painel/janela
		painel.setLayout(new GridLayout(4, 1));
		painelTitulo.setLayout(new FlowLayout());
		painelCadastro.setLayout(new FlowLayout());
		painelCompras.setLayout(new GridLayout(1, 1));
		painelAcoes.setLayout(new GridLayout());
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(painel, BorderLayout.CENTER);

		//Adicionar elementos
		painel.add(painelTitulo);
		painel.add(painelCadastro);
		painel.add(painelCompras);
		painel.add(painelAcoes);

		painelTitulo.add(labelTitulo);

		painelCadastro.add(labelFieldCliente);
		painelCadastro.add(comboBoxClientes);
		painelCadastro.add(btnCadastrar);

		painelCompras.add(scrollPane);
		scrollPane.setViewportView(listaCompras);

		painelAcoes.add(btnEditar);
		painelAcoes.add(btnAtualizar);

		//Configurações da janela
		setSize(500, 400);
		setLocationRelativeTo(null);

		//Mostrar janela
		setVisible(true);

		//Carregar clientes e adicioná-los ao combo box
		clientesCadastrados = gerenciadorBancoDeDados.carregarTodosClientes();

		for (Cliente c : clientesCadastrados) {
			//System.out.println(c.getNome());
			comboBoxClientes.addItem(c.getNome());
		}

		comboBoxClientes.setSelectedIndex(0);

		//Adicionar ações

		//Cadastrar nova compra
		btnCadastrar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				//Cliente selecionado
				int indexClienteSelecionado = comboBoxClientes.getSelectedIndex();
				Cliente clienteSelecionado = clientesCadastrados.elementAt(indexClienteSelecionado);

				Compra novaCompra = new Compra();
				novaCompra.setIdCliente(clienteSelecionado.getId());

				Vector<Integer> resultado = gerenciadorBancoDeDados.salvar(novaCompra);

				int result = resultado.elementAt(0);
				int chavePrimariaCompraSalva = resultado.elementAt(1);

				if(result != 0) {
					JOptionPane.showMessageDialog(null, "Compra cadastrada com sucesso! A seguir você poderá adicionar produtos a esta compra.");
					carregarCompras();

					//Carregar compra a ser editada
					Compra compraEditar = gerenciadorBancoDeDados.carregarCompra(chavePrimariaCompraSalva);

					editarCompra(compraEditar);

				}else {
					JOptionPane.showMessageDialog(null, "Erro no cadastro! Tente novamente");
				}
			}
		});

		//Editar compra selecionada
		btnEditar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				//Obter compra selecionada
				int indexSelecionado = listaCompras.getSelectedIndex();
				Compra compraSelecionada = comprasCadastradas.elementAt(indexSelecionado);

				editarCompra(compraSelecionada);

			}
		});

		//Atualizar lista de compras
		btnAtualizar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				carregarCompras();

			}
		});




		//Carregar lista de compras
		carregarCompras();

	}


	public void carregarCompras() {

		comprasCadastradas = gerenciadorBancoDeDados.carregarTodasCompras();
		//Limpar jlist
		listaCompras.removeAll();

		DefaultListModel<String> listModel = new DefaultListModel<String>();

		for (Compra c : comprasCadastradas) {
			listModel.addElement(c.getNomeCliente() + " [Total produtos: "+ c.getTotalProdutos() +"]");
		}

		listaCompras.setModel(listModel);
		listaCompras.setSelectedIndex(0);

	}


	//Editar compra com a chave primaria
	public void editarCompra(Compra compra) {

		JanelaEditCompras jec = new JanelaEditCompras(compra);

	}

}

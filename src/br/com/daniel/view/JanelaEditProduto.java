package br.com.daniel.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.com.daniel.database.DatabaseManager;
import br.com.daniel.model.Produto;

public class JanelaEditProduto extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1958820995559045196L;
	private JPanel painel;
	private JPanel painelTitulo;
	private JPanel painelEdicao;
	private JPanel painelAcaoSalvar;
	private JLabel labelTitulo;
	private JLabel labelFieldNome;
	private JLabel labelFieldValor;
	private JTextField textFieldNomeProduto;
	private JTextField textFieldValorProduto;
	private JButton btnSalvar;
	private DatabaseManager gerenciadorBancoDeDados;
	private Produto produto;

	public JanelaEditProduto(Produto produtoEditar){
		super("Editar Produto");

		//Banco
		gerenciadorBancoDeDados = new DatabaseManager();

		//Iniciar elementos da janela
		painel = new JPanel();
		painelTitulo = new JPanel();
		painelEdicao = new JPanel();
		painelAcaoSalvar = new JPanel();
		labelTitulo = new JLabel("Gerenciar Clientes");
		labelFieldNome = new JLabel("Nome: ");
		labelFieldValor = new JLabel("Valor: ");
		textFieldNomeProduto = new JTextField("", 12);
		textFieldValorProduto = new JTextField("", 6);
		btnSalvar = new JButton("Salvar alterações");

		produto = produtoEditar;

		//Configurar painel/janela
		painel.setLayout(new GridLayout(4, 1));
		painelTitulo.setLayout(new FlowLayout());
		painelEdicao.setLayout(new FlowLayout());
		painelAcaoSalvar.setLayout(new GridLayout());
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(painel, BorderLayout.CENTER);

		//Adicionar elementos
		painel.add(painelTitulo);
		painel.add(painelEdicao);
		painel.add(painelAcaoSalvar);

		painelTitulo.add(labelTitulo);

		painelEdicao.add(labelFieldNome);
		painelEdicao.add(textFieldNomeProduto);
		painelEdicao.add(labelFieldValor);
		painelEdicao.add(textFieldValorProduto);
		
		painelAcaoSalvar.add(btnSalvar);

		//Configurações da janela
		setSize(500, 200);
		setLocationRelativeTo(null);

		//Mostrar janela
		setVisible(true);

		//Preencher campos
		textFieldNomeProduto.setText(produto.getNomeProduto());
		textFieldValorProduto.setText(Float.toString(produto.getValorProduto()));
		
		//Ação de atualização
		btnSalvar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if(textFieldNomeProduto.getText().equals("")) {

					JOptionPane.showMessageDialog(null, "Por favor informe o nome do produto");

				}else {

					produto.setNomeProduto(textFieldNomeProduto.getText());
					produto.setValorProduto(Float.valueOf(textFieldValorProduto.getText()));
					
					int result = gerenciadorBancoDeDados.update(produto);

					if(result != 0) {
						JOptionPane.showMessageDialog(null, "Produto atualizado com sucesso!");
						dispose();
					}else {
						JOptionPane.showMessageDialog(null, "Erro no update! Tente novamente");
					}

				}

			}
		});

	}

}

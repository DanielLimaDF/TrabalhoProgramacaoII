package br.com.daniel.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class JanelaInicial extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8332509983342233157L;
	private JPanel painel;
	private JLabel labelTitulo;
	private JButton btnClientes;
	private JButton btnProdutos;
	private JButton btnCompras;
	
	public JanelaInicial() {
		super("Sistema de Compras");
		
		//Iniciar elementos da janela
		painel = new JPanel();
		labelTitulo = new JLabel("Sistema de Compras");
		btnClientes = new JButton("Clientes");
		btnProdutos = new JButton("Produtos");
		btnCompras = new JButton("Compras");
		
		//Configurar painel/janela
		painel.setLayout(new GridLayout(4, 1));
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(painel, BorderLayout.CENTER);
		
		//Adicionar elementos
		painel.add(labelTitulo);
		painel.add(btnClientes);
		painel.add(btnProdutos);
		painel.add(btnCompras);
		
		//Configurações da janela
		setSize(500, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Mostrar janela
		setVisible(true);
		
		//Adicionar ações
		btnClientes.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Abrindo janela do cliente...");
				JanelaClientes jc = new JanelaClientes();
				
			}
		});
		
		btnProdutos.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Abrindo janela de produtos...");
				JanelaProdutos jp = new JanelaProdutos();
				
			}
		});

		btnCompras.addActionListener(new ActionListener() {
	
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Abrindo janela de compras...");
				JanelaCompras jc = new JanelaCompras();
				
			}
		});
		
		
		
		
		
		
		
	}
	
}
